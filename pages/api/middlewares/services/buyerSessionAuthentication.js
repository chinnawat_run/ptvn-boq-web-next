import { AppType } from '../../../../src/commons/constants';
import { decodeToken } from '../../../../src/api/token';
import activateSession from '../../../../src/api/gateways/BuyerGateway';

const getToken = req => req.cookies.buyer_auth;

const authenticateBuyer = async (url, token) => {
  const activationResult = await activateSession(url, token);
  const { result, message } = activationResult;
  return {
    success: result === 'success',
    message
  };
};

const authenticate = async (token) => {
  if (!token) {
    return {
      success: false,
      message: 'Token not found.'
    };
  }

  const payload = decodeToken(token);
  if (!payload || !payload.data) {
    return {
      success: false,
      message: 'Invalid Token.'
    };
  }
  let data;
  try {
    data = JSON.parse(payload.data);
  } catch (e) {
    data = '';
  }
  if (!data) {
    return {
      success: false,
      message: 'Invalid Token.'
    };
  }
  const { appType } = data;
  if (!appType) {
    return {
      success: false,
      message: 'App type not found in token.'
    };
  }
  if (appType !== AppType.BUYER) {
    return {
      success: false,
      message: 'Token and App types mismatched.'
    };
  }
  const { updateSessionUrl } = data;
  if (!updateSessionUrl) {
    return {
      success: false,
      message: 'Client session-update url not found in token.'
    };
  }

  const result = await authenticateBuyer(updateSessionUrl, token);
  return { ...result };
};

export default { getToken, authenticate };
