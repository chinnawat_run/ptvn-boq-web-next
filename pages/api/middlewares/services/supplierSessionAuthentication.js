// import { AppType } from '../../../../src/commons/constants';
// import { decodeToken } from '../../../../src/api/token';
// import SupplierGateway from '../../gateways/SupplierGateway';

// const { checkIfSessionIsActive, verifySession } = SupplierGateway;

// /** @namespace req.cookies.supplier_auth */
// const getToken = req => req.cookies.supplier_auth;

// const checkActiveSession = async (userGUID) => {
//   const sessionActiveCheckResult = await checkIfSessionIsActive(userGUID);
//   const { result, data } = sessionActiveCheckResult;
//   return {
//     active: (result && result.Code === '200' && !!data && !!data[0] && data[0].IsSessionTimeout === 0)
//   };
// };

// const checkValidSession = async (userGUID) => {
//   const sessionVerificationResult = await verifySession(userGUID);
//   const { result, data } = sessionVerificationResult;
//   return {
//     valid: (result && result.Code === '200' && !!data && !!data[0] && !!data[0].UserGUID)
//   };
// };

// const authenticateSupplier = async (userGUID) => {
//   const result = await Promise.all([checkActiveSession(userGUID), checkValidSession(userGUID)]);
//   let message = !result[0].active ? 'Session timeout.' : '';
//   message += !result[1].valid ? 'Session is not valid' : '';
//   return {
//     success: result[0].active && result[1].valid,
//     message
//   };
// };

// const authenticate = async (token) => {
//   if (!token) {
//     return {
//       success: false,
//       message: 'Token not found.'
//     };
//   }
//   const payload = decodeToken(token);
//   if (!payload || !payload.data) {
//     return {
//       success: false,
//       message: 'Invalid Token.'
//     };
//   }
//   let data;
//   try {
//     data = JSON.parse(payload.data);
//   } catch (e) {
//     data = '';
//   }
//   if (!data) {
//     return {
//       success: false,
//       message: 'Invalid Token.'
//     };
//   }
//   const { appType } = data;
//   if (!appType) {
//     return {
//       success: false,
//       message: 'App type not found in token.'
//     };
//   }
//   if (appType !== AppType.SUPPLIER) {
//     return {
//       success: false,
//       message: 'Token and App types mismatched.'
//     };
//   }
//   const { userGuid } = data;
//   if (!userGuid) {
//     return {
//       success: false,
//       message: 'User session id not found in token.'
//     };
//   }
//   const result = await authenticateSupplier(userGuid);
//   return { ...result };
// };

// export default { getToken, authenticate };
