import buyerSessionAuthentication from './buyerSessionAuthentication';
import supplierSessionAuthentication from './supplierSessionAuthentication';

export default {
  buyerSessionAuthentication,
  supplierSessionAuthentication
};
