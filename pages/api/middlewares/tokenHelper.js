import { AppType } from '../../../src/commons/constants';
import services from './services';

const getAllAssociatedTokens = req => ({
  buyerToken: services.buyerSessionAuthentication.getToken(req),
  // supplierToken: services.supplierSessionAuthentication.getToken(req)
});

const tokenHelper = handler => async (req, res, next) => {
  console.log('token helper ....')
  const appType = req.cookies.app_type;
  const { buyerToken, supplierToken } = getAllAssociatedTokens(req);
  if (appType === AppType.BUYER) {
    req.headers.token = buyerToken;
    // next();
    return handler(req, res);
  // } else if (appType === AppType.SUPPLIER) {
  //   req.headers.token = supplierToken;
  //   // next();
  //   return handler(req, res);
  } else {
    res.status(403).send({ error: 'Undefined Application Type.' });
  }
};

export default tokenHelper;
