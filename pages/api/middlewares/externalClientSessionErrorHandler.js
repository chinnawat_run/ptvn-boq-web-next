const externalClientSessionErrorHandler = handler => async (req, res) => {
  console.log('external Client Session Error Handler ....')
  if (req.errorMessage) {
    if (req.errorName === 'FailedDependencyError') {
      res.status(424).send({ message: req.errorMessage });
    } else {
      res.status(401).send({ message: req.errorMessage });
    }
  }
  return handler(req, res);
};

export default externalClientSessionErrorHandler;
