import { AppType } from '../../../src/commons/constants';
import sessionAuthenticationService from './services';
import { decodeToken } from '../../../src/api/token';
import FailedDependencyError from '../../../src/api/errors/FailedDependencyError';

const {
  buyerSessionAuthentication: bsa,
  supplierSessionAuthentication: ssa
} = sessionAuthenticationService;

const externalClientSessionActivation = handler => async (req, res) => {
  console.log('external Client Session Activation ....')
  /** @namespace req.cookies.app_type */
  const appType = req.cookies.app_type;
  // Buyer
  if (appType === AppType.BUYER) {
    const token = bsa.getToken(req);
    bsa.authenticate(token)
      .then((result) => {
        if (result.success) {
          const payload = decodeToken(token);
          res.locals.buyerData = JSON.parse(payload.data);
          return handler(req, res)
        } else {
          req.errorMessage = result.message ? `buyer:[${result.message}]`: 'Unauthorized: Authentication failed.';
          return handler(req, res);
        }
      })
      .catch(() => {
        req.errorMessage = 'Unable to connect to EP to verify user';
        req.errorName = 'FailedDependencyError';
        // next(new FailedDependencyError('Unable to connect to EP to verify user'));
        return handler(req, res)
      });
    // Supplier
  // } else if (appType === AppType.SUPPLIER) {
  //   const token = ssa.getToken(req);
  //   ssa.authenticate(token)
  //     .then((result) => {
  //       if (result.success) {
  //         // next();
  //         return handler(req, res)
  //         const payload = decodeToken(token);
  //         res.locals.supplierData = JSON.parse(payload.data);
  //       } else {
  //         return handler(req, res)
  //         // next(new Error(result.message ? `supplier:[${result.message}]`
  //         //   : 'Unauthorized: Authentication failed.'));
  //       }
  //     })
  //     .catch(() => {
  //       return handler(req, res)
  //       // next(new FailedDependencyError('Unable to connect to EP to verify user'));
  //     });
  } else {
    req.errorMessage = 'Unknown Application Type.';
    return handler(req, res)
  }
};

export default externalClientSessionActivation;
