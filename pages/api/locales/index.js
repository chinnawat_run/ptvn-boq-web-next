import Promise from 'bluebird';
import dot from 'dot-object';
import baseRequest from '../../../src/api/request-default';
import config from '../../../src/config';

export default async (req, res) => {
  try {
    const data = await view(req);
    res.send(data);
  } catch (err) {
    res.send(err);
  }
}

// Get Option
const option = (req) => {
  const url = `${config.api.host}/api/languages/${req.query.lng}/${req.query.ns}`;

  return {
    method: 'GET',
    uri: url,
    json: true,
    resolveWithFullResponse: true
  };
};

const rejectModule = (resolve, reject, req, response = '') => {
  const importModule = req.body.import || false;
  if (importModule) {
    resolve(response);
  }

  if (response.statusCode === 406) {
    const errorResponse = {
      statusCode: response.statusCode,
      message: response.message
    };
    resolve(errorResponse);
  }

  reject(response);
};

const view = (req) => {
  return new Promise((resolve, reject) => {
    baseRequest(req)(option(req)).then(async (response) => {
      if (response.statusCode !== 200) {
        rejectModule(resolve, reject, req, response);
      } else {
        const data = response.body.data;
        dot.object(data);
        resolve(data);
      }
    }, (err) => {
      const errorResponse = {
        statusCode: err.statusCode,
        message: err.error
      };
      reject(errorResponse);
    });
  });
}
