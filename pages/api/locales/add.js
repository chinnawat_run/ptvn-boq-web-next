import Promise from 'bluebird';
import baseRequest from '../../../src/api/request-default';
import config from '../../../src/config';

export default async (req, res) => {
  try {
    const data = await create(req);
    res.send(data);
  } catch (err) {
    res.send(err);
  }
}

const option = (req) => {
  const url = `${config.api.host}/api/languages/${req.query.lng}/${req.query.ns}`;

  // remove key `_t`, is not necessary for store in database
  if (typeof req.body._t !== 'undefined') {
    delete req.body._t;
  }

  return {
    method: 'POST',
    uri: url,
    body: req.body,
    json: true,
    resolveWithFullResponse: true
  };
};

// Wrap data
const wrap = () => {
  const result = {
    data: 'ok'
  };
  return result;
};

const rejectModule = (resolve, reject, req, response = '') => {
  const importModule = req.body.import || false;
  if (importModule) {
    resolve(response);
  }

  if (response.statusCode === 406) {
    const errorResponse = {
      statusCode: response.statusCode,
      message: response.message
    };
    resolve(errorResponse);
  }

  reject(response);
};

const create = (req) => {
  return new Promise((resolve, reject) => {
    baseRequest(req)(option(req)).then(async (response) => {
      if (response.statusCode !== 201) {
        rejectModule(resolve, reject, req, response);
      } else {
        resolve(await wrap(response.body));
      }
    }, (err) => {
      const errorResponse = {
        statusCode: err.statusCode,
        message: err.error
      };
      reject(errorResponse);
    });
  });
}
