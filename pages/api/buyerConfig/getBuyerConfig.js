import clientSessionHandler from '../middlewares';
import tokenHelper from '../middlewares/tokenHelper';
import request from 'request-promise';
import config from '../../../src/config';

const { clientSessionActivation, clientSessionErrorHandler } = clientSessionHandler;

const getBuyerConfig = async (req, res) => {
  try {
    const config = await getConfig(req);
    res.send(config);
  } catch (err) {
    res.send(err);
  }
}

function getOption(req) {
  const api = config.api.host;
  const { buyerMpId } = req.query;
  const url = `${api}/api/buyerConfig`;

  const params = {
    buyerMpId
  };

  return {
    method: 'GET',
    uri: url,
    json: true,
    qs: params,
    headers: {
      token: req.headers.token
    },
    resolveWithFullResponse: true
  };
}

function rejectModule(resolve, reject, req, response = '') {
  const importModule = req.body.import || false;
  if (importModule) {
    resolve(response);
  }

  reject(response);
}

const getConfig = (req) => {
  return new Promise((resolve, reject) => {
    request(getOption(req)).then(async (response) => {
      if (response.statusCode === 200 || response.statusCode === 204) {
        resolve(response.body);
      }
      rejectModule(resolve, reject, req, response);
    }, (err) => {
      rejectModule(resolve, reject, req, err);
    });
  });
}

export default clientSessionActivation(clientSessionErrorHandler(tokenHelper(getBuyerConfig)))