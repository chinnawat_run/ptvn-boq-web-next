import { verifyTokenBuyer, decodeToken } from '../../../src/api/token';
import activateEPSession from '../../../src/api/gateways/BuyerGateway';
import { aadUserGateway } from '../../../src/api/gateways/AADUserGateway';
import { AppType } from '../../../src/commons/constants';

export default async (req,res) => {
  try {
    const result = await load(req);
    res.send(result);
  } catch (err) {
    res.send(err);
  }
}

const load = async (req) => {
  const tokenResult = {
    error: true,
    data: null,
    token: ''
  };

  const token = req.cookies.buyer_auth || req.body.buyer_auth;
  const appType = req.cookies.app_type || req.body.app_type;

  if (appType !== AppType.BUYER) {
    return tokenResult;
  }

  tokenResult.token = token;
  try {
    const activateResult = await activateSession(token);
    tokenResult.data = activateResult.data;
    if (activateResult.data) {
      if (!activateResult.error) {
        await syncUserWithRBAC({
          buyerID: activateResult.data.buyerId,
          username: activateResult.data.sysUserId,
          firstName: activateResult.data.name,
          email: activateResult.data.email,
          phone: activateResult.data.phone,
          eID: activateResult.data.eid
        });
        tokenResult.error = false;
      }
    }
    return tokenResult;
  } catch (err) {
    console.error(err);
    return tokenResult;
  }
}

/*
 Activate Session with EP
 */
const activateSession = async (token) => {
  if (!token) {
    throw new Error('Invalid parameter. token is null.');
  }

  const result = {
    error: true,
    data: null
  };

  let data = verifyTokenBuyer(token);
  if (data) {
    // Verified and correct
    data = JSON.parse(data.data);
    let success = false;
    try {
      const { result: activateResult } = await activateEPSession(data.updateSessionUrl, token);
      success = activateResult === 'success';
    } catch (err) {
      success = false;
    }
    if (success) {
      result.error = false;
    }
    result.data = data;
  } else {
    // Incorrect signature but we need data for further use
    data = decodeToken(token);
    if (data) {
      result.data = JSON.parse(data.data);
    }
  }
  return result;
};

/*
  Sync user with RBAC
  - Create new user if doesn't exist
  - Update user if already exists
 */
const syncUserWithRBAC = async (params) => {
  const { buyerID, username, firstName, lastName, email, phone, eID } = params;
  if (!eID || !username || !buyerID) {
    throw new Error('Invalid parameters.');
  }
  const user = await aadUserGateway.findUserByUsernameAndEID({ eID, username });
  if (!user) {
    await aadUserGateway.createUser({
      buyerID,
      username,
      firstName,
      lastName,
      email,
      phone,
      eID
    });
  } else {
    user.firstName = typeof firstName !== 'undefined' ? firstName : user.firstName;
    user.lastName = typeof lastName !== 'undefined' ? lastName : user.lastName;
    user.email = typeof email !== 'undefined' ? email : user.email;
    user.phone = typeof phone !== 'undefined' ? phone : user.phone;
    try {
      await aadUserGateway.updateUser(user.userId, user);
    } catch (err) {
      // Don't mind if updating was error. Let them use the app.
      console.warn(`Could not update userId ${user.userId}. -- ${err}`);
    }
  }
};
