import clientSessionHandler from '../middlewares';
import tokenHelper from '../middlewares/tokenHelper';
import request from 'request-promise';
import config from '../../../src/config';

const { clientSessionActivation, clientSessionErrorHandler } = clientSessionHandler;

const listMenu = async (req, res) => {
  try {
    const menu = await list(req);
    res.send(menu);
  } catch (err) {
    res.send(err);
  }
}

function getOption(req) {
  const api = config.api.host;
  const url = `${api}/api/menu`;
  const appType = req.body.appType;
  let permission = [];
  if (appType === 'PriceBookBuyer') {
    permission = req.body.permission;
  }
  const data = {
    permission,
    appType
  };

  const option = {
    method: 'POST',
    uri: url,
    body: data,
    json: true,
    resolveWithFullResponse: true
  };
  return option;
}

// Wrap data
async function wrapData(req, data) {
  const responseData = {
    statusCode: data.status,
    data: data.response
  };

  return responseData;
}

function rejectModule(resolve, reject, req, response = '') {
  const importModule = req.body.import || false;
  if (importModule) {
    resolve(response);
  }

  if (response.statusCode === 406) {
    const data = { data: [] };
    resolve(data);
  }

  reject(response);
}

const list = (req) => {
  return new Promise((resolve, reject) => {
    request(getOption(req))
      .then(async (response) => {
        if (response.statusCode !== 200) {
          rejectModule(resolve, reject, req, response);
        } else {
          resolve(await wrapData(req, response.body));
        }
      }, (err) => {
        rejectModule(resolve, reject, req, err);
      });
  });
}


export default clientSessionActivation(clientSessionErrorHandler(tokenHelper(listMenu)))
