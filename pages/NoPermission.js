import React from 'react'
import '../src/commons/assets/sass/frontend_style.scss';

export default function NoPermission() {
  return (
    <div>
      <div className="ac-message">
        <div className="oneplanet-logo"></div>
        <img src="/images/frontend/permission2x.png" alt={'Permission Denied'} className="thumbnail-empty permission" />
        <br />
        <span className="color-title font-xxl">Sorry, you don’t have permission to access this page.</span>
        <br />
      </div>
      <div className="ac-footer">©2018 Pantavanij.com</div>
    </div>
  )
}
