import React, { Component } from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types'
import Router from 'next/router';
import nookies from 'nookies';
import '../src/commons/assets/sass/frontend_style.scss';
import { AppType } from '../src/commons/constants';
import parser from 'urlencoded-body-parser';
import { setAppTypeAndBuyerAuth } from '../src/decorators/setCookies';
import loadAuthBuyerAndRequirePermission from '../src/decorators/requireAuthBuyer';
import LayoutBuyer from '../src/app/containers/Layout/LayoutBuyer';

export class ManagePricebook extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isAuthLoaded: false
    }
  }

  static getInitialProps = async (ctx) => {
    const { req, store } = ctx;
    const data = await parser(req);
    if (Object.keys(data).length !== 0 && data.constructor === Object) {
      setAppTypeAndBuyerAuth(ctx, data, AppType.BUYER);
    }
    const token = nookies.get(ctx).buyer_auth;
    let loadedAuthSuccess = await loadAuthBuyerAndRequirePermission(ctx, store, AppType.BUYER, token, 'mpb');

    return { loadedAuthSuccess };
  }

  static propTypes = {
    loadedAuthSuccess: PropTypes.bool.isRequired,
    loadMenu: PropTypes.func.isRequired
  }

  componentDidMount() {
    if (!this.props.loadedAuthSuccess) {
      Router.push('/NoPermission');
    }
  }

  render() {
    const { user, token, sharedAuth, authBuyer, menu } = this.props;
    return (
      <LayoutBuyer>
        <h1>Manage Pricebook</h1>
        <p>{JSON.stringify(user)}</p>
        <h3>token</h3>
        <p>{JSON.stringify(token)}</p>
        <hr />
        <h3>sharedAuth</h3>
        <p>{JSON.stringify(sharedAuth)}</p>
        <h3>authBuyer</h3>
        <p>{JSON.stringify(authBuyer)}</p>
        <h3>Menu</h3>
        <p>{JSON.stringify(menu)}</p>
      </LayoutBuyer>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.authBuyer.user,
    authAttempted: state.authBuyer.authAttempted,
    authAttempting: state.authBuyer.authAttempting,
    authSucceed: state.authBuyer.authSucceed,
    token: state.authBuyer.token,
    authBuyer: state.authBuyer,
    sharedAuth: state.sharedAuth,
    menu: state.menu
  }
};

export default connect(mapStateToProps, {
  
})(ManagePricebook)
