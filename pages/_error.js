import React from 'react'
import Head from 'next/head'
import { withRouter } from 'next/router'
import '../src/commons/assets/sass/frontend_style.scss'


class ErrorPage extends React.Component {

  static propTypes() {
    return {
      errorCode: React.PropTypes.number.isRequired,
      url: React.PropTypes.string.isRequired
    }
  }

  static getInitialProps({ res, xhr }) {
    const errorCode = res ? res.statusCode : (xhr ? xhr.status : null)
    return { errorCode }
  }

  render() {
    var response
    switch (this.props.errorCode) {
      case 200: // Also display a 404 if someone requests /_error explicitly
      case 404:
        response = (
          <div>
            <Head>
              <title>Not Found</title>
            </Head>
            <div className="ac-message">
              <div className="oneplanet-logo"></div>
              <img src="/images/frontend/page_not_found2x.png" className="thumbnail-empty permission" alt="Page Not Found" /><br />
              <span
                className="color-title font-xxl">Sorry, this page can not be found.</span><br />
            </div>
          </div>
        )
        break
      case 500:
        response = (
          <div>
            
          </div>
        )
        break
      default:
        response = (
          <div>
            
          </div>
        )
    }

    return response
  }

}

export default withRouter(ErrorPage);

