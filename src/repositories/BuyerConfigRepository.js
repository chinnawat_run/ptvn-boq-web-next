import Repository from './Repository';

export default class BuyerConfigRepository extends Repository {
  constructor(options) {
    super(options);
    this.namespace = 'buyerConfig';
  }

  getBuyerConfig(buyerMpId) {
    return this.client.get(`${this.wrapperApi()}/getBuyerConfig?buyerMpId=${buyerMpId}`);
  }
}
export const buyerConfigRepository = new BuyerConfigRepository();
