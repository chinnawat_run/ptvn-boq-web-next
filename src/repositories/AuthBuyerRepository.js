import Repository from './Repository';
import axios from 'axios';

export class AuthBuyerRepository extends Repository {

  constructor(options) {
    super(options);
    this.namespace = 'buyer-session';
    this.url_LoadToken = `${this.wrapperApi()}/load-token`;
    this.url_Logout = `${this.wrapperApi()}/logout`;
    this.url_ExtendSession = `${this.wrapperApi()}/extend-session`;
  }

  async loadToken() {
    return axios.get(this.url_LoadToken, {
      withCredentials: true
    }).then(response => response).catch(error => error);
    // return this.client.get(this.url_LoadToken)
    //   .then(response => response)
    //   .catch(error => error);
  }

  async loadTokenByBody(buyer_auth, app_type) {
    return axios.post(this.url_LoadToken, {
      buyer_auth,
      app_type
    }, {
      withCredentials: true
    }).then(response => response).catch(error => error);
    // return this.client.get(this.url_LoadToken)
    //   .then(response => response)
    //   .catch(error => error);
  }

  async logout() {
    return this.client.get(this.url_Logout)
      .then(response => response)
      .catch(error => error);
  }

  async extendSession() {
    return this.client.get(this.url_ExtendSession)
      .then(response => response)
      .catch(error => error);
  }

}

export const authRepository = new AuthBuyerRepository();
