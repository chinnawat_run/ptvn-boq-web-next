// import config from '../../config/index';
import axios from 'axios';

export default class Repository {
  constructor(options = {}) {
    this.protocol = 'http';
    this.port = '3000';
    this.ebdApiServer = '';
    this.wrapperServer = `${this.protocol}://localhost:${this.port}`;
    this.apiServer = '';
    this.client = options.client || axios;
  }

  withClient(client) {
    if (this.client !== client) {
      this.client = client;
    }
    return this;
  }

  ebdApi() {
    return `${this.ebdApiServer}/api`;
  }

  api() {
    return `${this.apiServer}/api`;
  }

  wrapperApi() {
    return `${this.wrapperServer}/api/${this.namespace}`;
  }

  /**
   * Not accept null, undefined, '0'
   * @param obj
   * @returns {*}
   */
  cleanUnusedParams(obj) {
    Object.keys(obj).forEach((key) => {
      if (obj[key] === null || obj[key] === undefined || obj[key].toString() === '0') {
        delete obj[key];
      }
    });

    return obj;
  }
}

