import Repository from './Repository';

export default class MenuRepository extends Repository {
  constructor(options) {
    super(options);
    this.namespace = 'menu';
  }

  findAll(params) {
    return this.client.post(`${this.wrapperApi()}/list`, {
      appType: params.appType,
      permission: params.permission
    });
  }
}
export const menuRepository = new MenuRepository();
