export default {
  development: true,
  universal: true,
  secured: process.env.SECURED === 'true',
  client: {
    host: 'localhost',
    port: 3000
  },
  server: {
    current: !(process.env.CLIENT || false),
    host: 'localhost',
    port: 3010
  },
  ebdApi: {
    host: process.env.BASEURL_EBD_API_HOST,
    port: 3000
  },
  api: {
    host: process.env.BASEURL_API_HOST || 'localhost:',
    port: 3000
  },
  ga: {
    id: process.env.GA_TRACKING_ID || ''
  },
  redis: {
    host: process.env.REDIS_HOST || 'alpha-redis.oneplanets.com'
  },
  minio: {
    host: process.env.MINIO_HOST,
    port: process.env.MINIO_PORT,
    accessKey: process.env.MINIO_ACCESS_KEY,
    secretKey: process.env.MINIO_SECRET_KEY
  },
  session: {
    enable: true,
    intervalTime: 60000
  },
  url: {
    center_api: process.env.BASEURL_API_CENTER,
    rbac_api: process.env.BASEURL_API_RBAC,
    cookieDomain: process.env.COOKIE_DOMAIN,
    not_authorized: '/401',
    sww_url: process.env.BASE_API_SWW || 'http://supplierportal.demo.pantavanij.com/ServiceAPI'
  },
  uploadOption: {
    tempDest: '/tmp/'
  }
}