import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { translate } from '../../fork-components/react-i18next';
import { AppType } from '../../../commons/constants';
import BuyerLogoutForm from './BuyerLogoutForm';

import { setLanguage } from '../../../services/authBuyerService';

class Header extends Component {

  constructor(props) {
    super(props);

    this.state = {
      navOpen: true,
      popupLanguage: false,
      popupAlert: false,
      popupProfile: false
    };
  }

  componentDidMount() {
    const { userBuyer, userSupplier, i18n } = this.props;
    if (userBuyer) {
      i18n.changeLanguage(userBuyer.language);
    } else if (userSupplier) {
      i18n.changeLanguage(userSupplier.language);
    }
  }

  ctrlNav = () => {
    this.setState({ navOpen: !this.state.navOpen });
  }

  handleClick = (e) => {
    if (e != null) {
      if (this.state.LanguageArea.contains(e.target)) {
        this.setState({
          popupLanguage: !this.state.popupLanguage,
          popupAlert: false,
          popupProfile: false
        });
        document.addEventListener('click', this.handleOutsideClick, false);
      } else if (this.state.ProfileArea.contains(e.target)) {
        this.setState({
          popupLanguage: false,
          popupAlert: false,
          popupProfile: !this.state.popupProfile
        });
        document.addEventListener('click', this.handleOutsideClick, false);
      }
    } else {
      this.setState({ popupLanguage: false });
      this.setState({ popupAlert: false });
      this.setState({ popupProfile: false });
      document.removeEventListener('click', this.handleOutsideClick, false);
    }
  }

  LanguageArea = (node) => {
    this.setState({ LanguageArea: node });
  };

  AlertArea = (node) => {
    this.setState({ AlertArea: node });
  };

  ProfileArea = (node) => {
    this.setState({ ProfileArea: node });
  };

  handleOutsideClick = (e) => {
    // ignore clicks on the component itself
    if ((this.state.LanguageArea && this.state.LanguageArea.contains(e.target)) ||
      (this.state.ProfileArea && this.state.ProfileArea.contains(e.target))) {
      return;
    }

    this.handleClick();
  }

  handleSignOut = (e) => {
    e.preventDefault();
    if (this.props.appType === AppType.SUPPLIER) {
      this.props.logoutSupplier();
    }
  }

  changeLanguageBuyer = (lang) => {
    const { changeLanguageBuyer, i18n } = this.props;
    changeLanguageBuyer(lang);
    i18n.changeLanguage(lang);
    this.setState({ popupLanguage: false });
    document.removeEventListener('click', this.handleOutsideClick, false);
  }

  renderSignOut = () => {
    const { t } = this.props;
    if (this.props.appType === AppType.BUYER) {
      return (<BuyerLogoutForm signOutMessage={t('priceBookCommon:signOut', { defaultValue: 'Sign Out' })} />);
    }
    return (<a href="" onClick={this.handleSignOut} className="logout">{t('priceBookCommon:signOut', { defaultValue: 'Sign Out' })}</a>);
  }

  render() {
    const { actionLeftMenu, appType, userBuyer, userSupplier, logoImage } = this.props;
    const name = appType === AppType.BUYER ? userBuyer.name : userSupplier.username;
    const nameSpace = appType === AppType.BUYER ? 'priceBookBuyer' : 'priceBookSupplier';
    const language = appType === AppType.BUYER ? userBuyer.language : userSupplier.language;
    // let logo = '';
    // if (logoImage) {
    //   logo = `/images/header/ ${logoImage}`;
    // }

    return (
      <div>
        <div className="fe-op-header">
          <div className="op-inner">
            <a onClick={actionLeftMenu} className="navbar-toggle">
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar" />
              <span className="icon-bar" />
              <span className="icon-bar" />
            </a>
            <a href="" className="op-logo" style={{ backgroundImage: `url(/images/header/${logoImage})` }}>&nbsp;</a>
            <div className="pull-right">
              <div className={`box-dropdown lang-dropdown ${this.state.popupLanguage ? 'open' : ''}`} ref={this.LanguageArea} >
                <div onClick={this.handleClick} className="box-dropdown-title nav-format lang-selected-box">
                  <span className={`lang-flag icon-flag-${language === 'en' ? 'us' : 'th'}`} />
                </div>
                <div className="box-dropdown-list lang-dropdown-list">
                  {nameSpace === 'priceBookBuyer' &&
                  <ul>
                    <li className={language === 'en' ? 'selected' : ''}>
                      <a onClick={() => this.changeLanguageBuyer('en')} title="">
                        <span className={`space-select ${language === 'en' ? 'icon-check' : ''}`} />
                        <span className="lang-flag icon-flag-us" />
                        <span className="lang-text">English (United States)</span>
                      </a>
                    </li>
                    {/* <li className={language === 'th' ? 'selected' : ''}>
                      <a onClick={() => this.changeLanguageBuyer('th')} title="ภาษาไทย (Thailand)">
                        <span className={`space-select ${language === 'th' ? 'icon-check' : ''}`} />
                        <span className="lang-flag icon-flag-th" />
                        <span className="lang-text">ภาษาไทย (Thailand)</span>
                      </a>
                    </li> */}
                  </ul>
                  }
                  {/* {nameSpace === 'priceBookSupplier' &&
                  <ul>
                    <li className={language === 'en' ? 'selected' : ''}>
                      <a onClick={() => this.changeLanguageSupplier('en')} title="">
                        <span className={`space-select ${language === 'en' ? 'icon-check' : ''}`} />
                        <span className="lang-flag icon-flag-us" />
                        <span className="lang-text">English (United States)</span>
                      </a>
                    </li>
                    <li className={language === 'th' ? 'selected' : ''}>
                      <a onClick={() => this.changeLanguageSupplier('th')} title="ภาษาไทย (Thailand)">
                        <span className={`space-select ${language === 'th' ? 'icon-check' : ''}`} />
                        <span className="lang-flag icon-flag-th" />
                        <span className="lang-text">ภาษาไทย (Thailand)</span>
                      </a>
                    </li>
                  </ul>} */}
                </div>
              </div>

              <div ref={this.AlertArea} style={{ display: 'none' }}></div>
              <div className={`box-dropdown user-set-top box-dropdown-profile ${this.state.popupProfile ? 'open' : ''}`} ref={this.ProfileArea} >
                <div onClick={this.handleClick} className="box-dropdown-title nav-format nav-profile">
                  <span className="icon-user-set icon-profile" />
                  <div className="profile-name">
                    <span>{name}</span>
                    <i className="icon-arrow-menu-active" />
                  </div>
                </div>
                <div className="box-dropdown-list">
                  <ul>
                    <li className="user-dd-display">
                      <span className="font-heading color-title">{name}</span>
                      {appType !== AppType.BUYER && <br />}
                      {appType !== AppType.BUYER &&
                      <span className="color-sub">{userSupplier.companyName}</span>
                      }
                    </li>
                    <li>{this.renderSignOut()}</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Header.defaultProps = {
  userBuyer: null
};

Header.propTypes = {
  actionLeftMenu: PropTypes.func.isRequired,
  userBuyer: PropTypes.object,
  t: PropTypes.func.isRequired,
  i18n: PropTypes.object.isRequired,
  changeLanguageBuyer: PropTypes.func.isRequired,
  appType: PropTypes.string.isRequired
};

export default connect(
  state => ({
    userBuyer: state.authBuyer.user,
    appType: state.sharedAuth.appType,
    logoImage: state.buyerConfig.data.logoImage
  }),
  {
    changeLanguageBuyer: setLanguage
  }
)(translate(['priceBookCommon'])(Header));
