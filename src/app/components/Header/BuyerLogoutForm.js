import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';

import { logout } from '../../../services/authBuyerService';

class BuyerLogoutForm extends Component {
  constructor(props) {
    super(props);

    this.submitForm = this.submitForm.bind(this);
  }

  submitForm(e) {
    e.preventDefault();
    const form = e.target;
    this.props.logout()
      .then(() => {
        form.submit();
      });
  }

  render() {
    const { signOutMessage } = this.props;
    return (
      <form action={this.props.user.logoutUrl} onSubmit={this.submitForm} method={'POST'}>
        <input name={'token'} type={'hidden'} value={this.props.token} />
        <button type={'submit'} className="logout">{signOutMessage}</button>
      </form>
    );
  }
}

BuyerLogoutForm.propTypes = {
  token: PropTypes.string.isRequired,
  user: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
  signOutMessage: PropTypes.string.isRequired
};

export default connect(
  state => ({
    user: state.authBuyer.user,
    token: state.authBuyer.token
  }),
  {
    logout
  }
)(BuyerLogoutForm);

