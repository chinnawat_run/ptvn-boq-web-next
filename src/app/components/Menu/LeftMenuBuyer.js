import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { translate } from '../../fork-components/react-i18next';
import { loadMenu } from '../../../services/menuService';
import BuyerGoToHomeForm from './BuyerGoToHomeForm';
import Router from 'next/router'

class LeftMenuBuyer extends Component {

  state = {
    subMenu: {},
    menuName: '',
    menuNameLv2: ''
  };

  componentDidMount() {
    this.props.loadMenu();
    this.activeMenu();
  }

  handleMenu = (menu) => {
    const { toggleMenu } = this.props;
    if (menu.MenuType === 'LABEL') {
      this.setState({ subMenu: menu.subMenu, menuName: menu.Name.toLowerCase() }, () =>
        toggleMenu(true, true));
    }
  }

  submitFormHome = () => {
    document.getElementById('goHome').submit();
  }

  handleMenuLevel2 = (subMenu) => {
    if (subMenu.MenuType === 'LINK') {
      if (window.location.pathname === subMenu.Link) {
        Router.reload();
      } else {
        Router.push(`${subMenu.Link}`)
      }
      this.handleScale();
    }
  }

  handleScale = () => {
    const { toggleMenu, isScale } = this.props;
    if (isScale) {
      toggleMenu(false, false);
    } else {
      toggleMenu(true, false);
    }
    this.activeMenu();
  }

  closeBoxOverlay = () => {
    this.setState({ subMenu: {}, menuName: '' }, () => {
      this.handleScale();
    });
  }

  activeMenu = () => {
    const pathName = window.location.pathname;
    if (pathName === '/ManagePriceBook') {
      this.setState({ menuName: 'price book', menuNameLv2: 'Manage Pricebook' });
    } else if (pathName === '/ManageSupplierGroup') {
      this.setState({ menuName: 'price book', menuNameLv2: 'Manage Supplier Group' });
    } else if (pathName === '/ManagePriceModelGroup') {
      this.setState({ menuName: 'price book', menuNameLv2: 'Manage Price Model Group' });
    }
  }

  render() {
    const { t, navLv1Open, navLv2Open, menuList } = this.props;
    const { subMenu, menuName, menuNameLv2 } = this.state;
    const isLevel1 = navLv1Open ? 'block' : 'none';
    const isLevel2 = navLv2Open ? 'block' : 'none';
    return (
      <div>
        <div id="navSlide" className="op-nav-left-icon" style={{ display: `${isLevel1}` }}>
          <div id="ul-left-list" className="nav-left-inner">
            <BuyerGoToHomeForm />
            <ul className="fe-nav-list">
              {
                menuList && menuList.map((item, refId) => {
                  const isActiveMenu = menuName.toLowerCase() === item.Name.toLowerCase() ? 'active' : '';
                  const linkGoToHome = {
                    onClick: item.Name && item.Name === 'HOME' ? (e) => { e.preventDefault(); this.submitFormHome(); } : (e) => { e.preventDefault(); this.handleMenu(item); }
                  };
                  return (
                    <li key={`menu_${String(refId)}`}>
                      <a {...linkGoToHome} className={`head-go ${isActiveMenu}`} data-rb={`${item.Name}_menu`}>
                        <span className={item.Icon} />
                        <span className="title">{t(`${item.TranslationKey}`, { defaultValue: `${item.Name}` })}</span>
                        <div className="tooltip-left">
                          <span>{t(`${item.TranslationKey}`, { defaultValue: `${item.Name}` })}</span>
                        </div>
                      </a>
                    </li>
                  );
                })}
            </ul>
          </div>
        </div>

        {subMenu.menuLevel2 && subMenu.menuLevel2.length > 0 &&
          <div id="overlay-menu" style={{ display: `${isLevel2}` }}>
            <span className="header-menu">{t(`${subMenu.header.TranslationKey}`, { defaultValue: `${subMenu.header.Name}` })}
              <i className="icon-cross" onClick={() => this.closeBoxOverlay()} data-rb="close_menu" />
            </span>
            <div className="clearfix" />
            <ul id="header-submenu">
              {
                subMenu.menuLevel2 && subMenu.menuLevel2.map((subMenuList) => {
                  const isActiveMenu2 = menuNameLv2.toLowerCase() === subMenuList.Name.toLowerCase() ? 'active' : '';
                  return (
                    <li key={`${subMenuList.Name}`} className={subMenuList.MenuType === 'LABEL' ? 'list-menu-head' : 'list-menu'}>
                      <a className={isActiveMenu2} onClick={() => this.handleMenuLevel2(subMenuList)} data-rb={`${subMenuList.Name}_menu_lv2`}>
                        {t(`${subMenuList.TranslationKey}`, { defaultValue: `${subMenuList.Name}` })}</a>
                    </li>
                  );
                })
              }
            </ul>
          </div>
        }
        <div id="overlay-box" onClick={() => this.closeBoxOverlay()} style={{ display: `${isLevel2}` }} />
      </div>

    );
  }
}


LeftMenuBuyer.defaultProps = {
  language: 'en',
  pathName: ''
};

LeftMenuBuyer.propTypes = {
  t: PropTypes.func.isRequired,
  menuList: PropTypes.array.isRequired,
  navLv1Open: PropTypes.bool.isRequired,
  navLv2Open: PropTypes.bool.isRequired,
  loadMenu: PropTypes.func.isRequired,
  toggleMenu: PropTypes.func.isRequired,
  isScale: PropTypes.bool.isRequired
};

export default connect(
  state => ({
    navLv1Open: state.layout.navLv1Open,
    navLv2Open: state.layout.navLv2Open,
    menuList: state.menu.data
  }),
  {
    loadMenu
  }
)(translate(['priceBookCommon'])(LeftMenuBuyer));
