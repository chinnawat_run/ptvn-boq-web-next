import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { logout } from '../../../services/authBuyerService';

class BuyerGoToHomeForm extends Component {

  submitForm(e) {
    e.preventDefault();
    const form = e.target;
    this.props.logout()
      .then(() => {
        form.submit();
      });
  }

  render() {
    return (
      <form id={'goHome'} action={this.props.user.url} onSubmit={this.submitForm} method={'POST'} className={'form-menu'}>
        <input name={'token'} type={'hidden'} value={this.props.token} />
      </form>
    );
  }
}

BuyerGoToHomeForm.propTypes = {
  token: PropTypes.string.isRequired,
  user: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired
};

export default connect(
  state => ({
    user: state.authBuyer.user,
    token: state.authBuyer.token
  }),
  {
    logout
  }
)(BuyerGoToHomeForm);

