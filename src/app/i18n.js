import i18next from './fork-components/i18next';
import XHR from './fork-components/i18next-xhr-backend';

const i18n = i18next;

i18n
  .use(XHR)
  .init({
    fallbackLng: 'en',
    debug: false,
    ns: ['common'],
    defaultNS: 'common',
    backend: {
      loadPath: '/api/locales?lng={{lng}}&ns={{ns}}',
      addPath: '/api/locales/add?lng={{lng}}&ns={{ns}}'
    },
    interpolation: {
      escapeValue: false, // not needed for react!!
      formatSeparator: ',',
      format(value, format) {
        if (format === 'uppercase') return value.toUpperCase();
        return value;
      }
    },
    // react i18next special options (optional)
    react: {
      wait: true, // set to true if you like to wait for loaded in every translated hoc
      nsMode: 'default' // set it to fallback to let passed namespaces to translated hoc act as fallbacks
    },
    saveMissing: true,
    saveMissingTo: 'all'
  });

export default i18n;
