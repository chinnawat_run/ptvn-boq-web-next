import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import browser from 'detect-browser';
import i18n from '../../i18n'; // initialized i18next instance
import { I18nextProvider } from '../../fork-components/react-i18next';
import Header from '../../components/Header';

import { toggleNavMenu } from '../../../services/layoutService';
// import { action as authAction } from '../../../services/authBuyerService';
// import { action as buyerAppAction } from '../../../services/buyerAppService';
// import ReactInterval from '../../components/Common/ReactInterval';
// import GlobalLoadingIndicator from '../../components/Common/GlobalLoadingIndicator';
// import config from '../../../../config/index';
// import BuyerSessionExpiredModal from '../../components/Modals/BuyerSessionExpiredModal';
// import UnableToAccessModal from '../../components/Modals/UnableToAccessModal';
import LeftMenuBuyer from '../../components/Menu/LeftMenuBuyer';

class LayoutBuyer extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isScale: false,
      enableSessionInterval: true, // config.session.enable
      intervalTimeout: 60 // Number(config.session.intervalTime)
    };
  }

  componentDidMount() {
    this.updateDimensions();

    if (!window.addEventListener && window.attachEvent) {
      window.attachEvent('onresize', this.updateDimensions);
    } else {
      window.addEventListener('resize', this.updateDimensions);
    }
  }

  componentDidUpdate(prevProp, prevState) {
    const { isScale } = this.state;
    const { navLv1Open } = this.props;


    if (prevProp.navLv1Open !== navLv1Open) {
      if (isScale && navLv1Open) {
        document.getElementById('boxOverlay').classList.add('show');
      } else {
        document.getElementById('boxOverlay').classList.remove('show');
      }
    }

    if (prevState.isScale !== isScale) {
      if (!isScale) {
        document.getElementById('boxOverlay').classList.remove('show');
      }
    }
  }

  componentWillUnmount() {
    if (!window.removeEventListener && window.detachEvent) {
      window.detachEvent('onresize', this.updateDimensions);
    } else {
      window.removeEventListener('resize', this.updateDimensions);
    }
  }

  updateDimensions = () => {
    const { toggleMenu } = this.props;
    const width = window.innerWidth ||
      document.documentElement.clientWidth ||
      document.body.clientWidth;

    const scale = width <= 1160;

    if (scale) {
      toggleMenu(false, false);
    } else {
      toggleMenu(true, false);
    }

    this.setState({ isScale: scale });
  };

  actionLeftMenu = () => {
    const { toggleMenu, navLv1Open, navLv2Open } = this.props;
    toggleMenu(!navLv1Open, !navLv2Open);
  }

  checkUserSession = () => {
    this.props.checkUserSession();
  };

  render() {
    const { toggleMenu } = this.props;
    const { isScale } = this.state;

    let classForIE8 = '';
    const pageContentStyle = {};

    if (isScale) {
      if (browser.name === 'ie' && (browser.version === '8.0.0' || browser.version === '9.0.0')) {
        classForIE8 = 'bodyfix';
        pageContentStyle.marginLeft = '0px';
      }
    }

    return (
      <I18nextProvider i18n={i18n}>
        <div id="bodyContainer" className={`body-container ${classForIE8}`}>
          {/* <GlobalLoadingIndicator> */}
          <Header actionLeftMenu={this.actionLeftMenu} />
          {/* <ReactInterval
              timeout={this.state.intervalTimeout} enabled={this.state.enableSessionInterval}
              callback={() => this.checkUserSession()} /> */}
          {/* end header */}
          <LeftMenuBuyer
            // navMenuOpen={this.props.navMenuOpen}
            // currentRoute={this.props.routes && this.props.routes[0].path}
            toggleMenu={toggleMenu}
            isScale={isScale}
          />
          <div id="boxOverlay" className="overlay" />
          {this.props.children}
          {/* </GlobalLoadingIndicator> */}
          {/* <BuyerSessionExpiredModal /> */}
          {/* <UnableToAccessModal
            isOpen={this.props.showUnableToAccessModal}
            onClick={this.props.hideUnableToAccessModal} /> */}
        </div>
      </I18nextProvider>
    );
  }
}

LayoutBuyer.propTypes = {
  children: PropTypes.array,
  navLv1Open: PropTypes.bool.isRequired,
  navLv2Open: PropTypes.bool.isRequired,
  toggleMenu: PropTypes.func.isRequired,
  hideUnableToAccessModal: PropTypes.func.isRequired
};

LayoutBuyer.defaultProps = {
  children: {}
};

export default connect(
  state => ({
    navLv1Open: state.layout.navLv1Open,
    navLv2Open: state.layout.navLv2Open,
    // showUnableToAccessModal: state.buyerApp.showUnableToAccessModal
  }),
  {
    toggleMenu: toggleNavMenu
    // toggleMenu: layoutAction('TOGGLE_NAV_MENU'),
    // checkUserSession: authAction('VERIFY_BUYER_USER'),
    // hideUnableToAccessModal: buyerAppAction('HIDE_UNABLE_TO_ACCESS')
  }
)(LayoutBuyer);
