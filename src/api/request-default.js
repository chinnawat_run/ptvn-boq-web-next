import request from 'request-promise';

const baseRequest = req => request.defaults({
  headers: { 'x-request-id': req.id }
});

export default baseRequest;
