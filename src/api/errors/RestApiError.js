export default class RestApiError extends Error {
  constructor(statusCode, message) {
    super(message);
    this.statusCode = statusCode;
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, RestApiError);
    }
    this.name = 'RestApiError';
  }
}
