export default class ResourceOccupiedError extends Error {
  constructor(message) {
    super(message);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ResourceOccupiedError);
    }
    this.name = 'ResourceOccupiedError';
  }
}

