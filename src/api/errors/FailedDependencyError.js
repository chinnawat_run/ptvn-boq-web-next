export default class FailedDependencyError extends Error {
  constructor(message) {
    super(message);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, FailedDependencyError);
    }
    this.name = 'FailedDependencyError';
  }
}

