import jwt from 'jsonwebtoken';
import config from './jwt_conf';

const SECRET = config.secret;

export function verifyTokenBuyer(token) {
  try {
    const data = jwt.verify(token, Buffer.from(SECRET, 'base64'));
    return data;
  } catch (error) {
    return false;
  }
}

export function decodeToken(token) {
  try {
    const data = jwt.decode(token);
    return data;
  } catch (error) {
    return false;
  }
}

export function generateToken(claims) {
  try {
    const token = jwt.sign(claims, Buffer.from(SECRET, 'base64'));
    return token;
  } catch (error) {
    return false;
  }
}
