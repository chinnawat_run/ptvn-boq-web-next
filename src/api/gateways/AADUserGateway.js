import request from 'request-promise';
import BaseRestGateway from './BaseRestGateway';

import config from '../../config';

export default class AADUserGateway extends BaseRestGateway {

  constructor() {
    super();
    this.baseUrl = config.url.rbac_api;
  }

  findUserByUsernameAndEID(params) {
    const qs = {
      username: params.username,
      eId: params.eID
    };
    const url = `${this.baseUrl}/api/users`;

    return request({
      method: 'GET',
      uri: url,
      json: true,
      headers: this.headers,
      qs,
      resolveWithFullResponse: true
    })
      .then((res) => {
        if (res.statusCode === 200 && res.body.data && res.body.data.length) {
          const user = res.body.data[0];
          if (user.attributes.eId === params.eID && user.attributes.username === params.username) {
            return user.attributes;
          }
          throw new Error('Retrieving user from Rbac failed');
        } else if (res.statusCode === 204) {
          return null;
        }
        throw new Error(res);
      });
  }

  createUser(params) {
    const body = {
      buyerId: params.buyerID,
      username: params.username,
      firstName: params.firstName,
      lastName: params.lastName,
      email: params.email,
      phone: params.phone,
      eId: params.eID,
      neverExpired: false
    };
    const url = `${this.baseUrl}/api/users`;

    return request({
      method: 'POST',
      uri: url,
      json: true,
      headers: this.headers,
      body,
      resolveWithFullResponse: true
    })
      .then((res) => {
        if (res.statusCode !== 201) {
          throw new Error(res);
        } else {
          return res.body.id;
        }
      });
  }

  updateUser(userID, user) {
    const url = `${this.baseUrl}/api/users/${userID}`;

    return request({
      method: 'PUT',
      uri: url,
      json: true,
      headers: this.headers,
      body: user,
      resolveWithFullResponse: true
    })
      .then((res) => {
        if (res.statusCode !== 200) {
          throw new Error(res);
        }
      })
      .catch((err) => {
        throw new Error(err);
      });
  }
}

export const aadUserGateway = new AADUserGateway();
