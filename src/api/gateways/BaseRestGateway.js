export default class BaseRestGateway {
  constructor(options = {}) {
    this.baseUrl = options.baseUrl;
    this.headers = {};
  }

  setHeader(name, data) {
    this.headers[name] = data;
    return this;
  }

}

