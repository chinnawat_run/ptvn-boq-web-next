import request from 'request-promise';
import config from '../../../src/config';

export default (url, token) => request(
  {
    method: 'GET',
    uri: url,
    headers: { token },
    json: true,
    rejectUnauthorized: !(config.ignoreUnauthorizedForSessionUpdate),
    resolveWithFullResponse: true
  })
  .then((response) => {
    return response.body;
  }).catch((err) => {
    const result = {
      result: 'failed',
      message: err.response.statusMessage
    };
    return result;
  });
