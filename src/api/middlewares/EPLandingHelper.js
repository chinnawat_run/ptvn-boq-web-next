// import config from '../../config';
const { AppType } = require('../../commons/constants');

module.exports.handleLanding = function (req, res, next) {
  try {
    const token = req.body.token
    res.cookie('app_type', AppType.BUYER, { maxAge: 86400 * 1000, httpOnly: true, secure: false });
    if (token !== undefined) {
      res.cookie('buyer_auth', token, { maxAge: 86400 * 1000, httpOnly: true, secure: false });
    }
  } catch (err) {
    // Ignore
  }

  next();
}
