const express = require('express');
const buyerSession = require('./buyer-session');

const api = express.Router();

api.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, token');

  if (true) {
    // Allow to use withCredentials when client requested from different origin
    res.header('Access-Control-Allow-Credentials', true);
    // Must set allow origin to specific domain when requested with withCredentials
    res.header('Access-Control-Allow-Origin', req.headers.origin);
  }

  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  } else {
    next();
  }
});

api.use('/buyer-session', buyerSession);

module.exports = api;