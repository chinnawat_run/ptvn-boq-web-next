/* Auth Buyer */
export const SET_TOKEN = 'set_token';
export const SET_AUTH_STATUS = 'set_auth_status';
export const SET_EXPIRED = 'set_expired';
export const SET_LANGUAGE = 'set_language';
export const UPDATE_USER = 'update_user';

/* Shared Auth */
export const SET_APP_TYPE = 'set_app_type'
export const SET_PERMISSIONS = 'set_permissions';

/* Menu */
export const SET_MENU = 'set_menu';

/* Layout */
export const TOGGLE_NAV_MENU = 'toggle_nav_menu';

/* Buyer Config */
export const SET_BUYER_CONFIG = 'set_buyer_config';