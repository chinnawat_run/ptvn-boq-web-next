import { TOGGLE_NAV_MENU } from './types';

const initialState = {
  navLv1Open: true,
  navLv2Open: false
}

export const toggleNavMenu = (navLv1Open, navLv2Open) => ({
  type: TOGGLE_NAV_MENU,
  payload: { navLv1Open, navLv2Open }
})


export default (state = initialState, { type, payload }) => {
  switch (type) {

  case TOGGLE_NAV_MENU:
    return { ...state, ...payload }

  default:
    return state
  }
}
