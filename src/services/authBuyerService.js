import { SET_AUTH_STATUS, UPDATE_USER, SET_TOKEN, SET_EXPIRED } from './types';
import { setAppType, setPermissions } from './sharedAuthService';
import { AppType } from '../commons/constants';

const initialState = {
  user: null,
  token: '',
  permissions: [],
  appType: '',
  expired: false,
  verificationFailedCount: 0
}

export const setBuyerUser = (params) => async (dispatch) => {
  if (params) {
    const user = {
      loginUrl: params.loginUrl,
      logoutUrl: params.logoutUrl,
      sid: params.sid,
      email: params.email,
      name: params.name,
      eid: params.eid,
      url: params.url,
      sysUserId: params.sysUserId,
      buyerMpID: params.buyerMpId,
      buyerID: params.buyerId,
      userID: params.sysUserId,
      language: params.language ? params.language : 'en',
      ebdUrl: params.ebdUrl,
      filter: params.filter,
      filterLoaded: false
    };
    const permissions = params.privileges.map(privilege => privilege.privilegeCode);
    dispatch(setAppType(AppType.BUYER));
    dispatch(setPermissions(permissions));
    return dispatch(updateUser(user));
  }
  return dispatch(updateUser(null));
};

export const extendSession = () => async (dispatch) => {
  try {
    await authRepository.extendSession();
  } catch (err) {
    if (err.response && err.response.status && err.response.status === 401) {
      dispatch(action('EXPIRE_SESSION')());
    } else if (err.response && err.response.status && err.response.status === 424) {
      dispatch(buyerAppAction('SHOW_UNABLE_TO_ACCESS')());
    } else {
      throw err;
    }
  }
}

export const logout = () => async () => {
  await authRepository.withClient($http).logout();
}

export const expireSession = () => (dispatch) => { return dispatch(setExpired(true)) };

export const verifyBuyerUser = () => async (dispatch) => {
  // verify user session
  try {
    const authVerifyResult = await buyerUserSessionRepository.withClient($http).verifyUserSession();
    if (authVerifyResult.status === 200) {
      if (authVerifyResult.data.result === 'success') {
        return dispatch(setExpired(false));
      } else if (authVerifyResult.data.result === 'timeout') {
        return dispatch(setExpired(true));
      }
      return dispatch(buyerAppAction('SHOW_UNABLE_TO_ACCESS')());
    }
  } catch (error) {
    return dispatch(buyerAppAction('SHOW_UNABLE_TO_ACCESS')());
  }
}

export const setLanguage = () => (dispatch, getState) => {
  const { authBuyer: { user } } = getState();
  user.language = lang;
  return dispatch(action('UPDATE_USER')(clone(user)));
}

export const setExpired = (expired) => ({
  type: type,
  payload: { expired }
})

export const setAuthStatus = (authStatus) => ({
  type: SET_AUTH_STATUS,
  payload: { authStatus }
});

export const setToken = (token) => ({
  type: SET_TOKEN,
  payload: { token }
});

export const updateUser = (user) => ({
  type: UPDATE_USER,
  payload: { user }
});


export default (state = initialState, { type, payload }) => {
  switch (type) {
    case UPDATE_USER:
      const { user } = payload;
      return { ...state, user }

    case SET_TOKEN:
      const { token } = payload;
      return { ...state, token }

    case SET_EXPIRED:
      return { ...state, ...payload };

    case SET_AUTH_STATUS:
      const { authStatus } = payload;
      return {
        ...state,
        authAttempted: typeof authStatus.authAttempted !== 'undefined' ? authStatus.authAttempted : state.authAttempted,
        authAttempting: typeof authStatus.authAttempting !== 'undefined' ? authStatus.authAttempting : state.authAttempting,
        authSucceed: typeof authStatus.authSucceed !== 'undefined' ? authStatus.authSucceed : state.authSucceed
      }

    default:
      return state
  }
}


