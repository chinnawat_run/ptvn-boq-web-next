import { SET_MENU } from "./types";
import { menuRepository } from '../repositories/MenuRepository';

const initialState = {
  data: []
}

export const loadMenu = () => async (dispatch, getState) => {
  const {
    sharedAuth: { appType, permissions }
  } = getState();
  const params = {
    appType,
    permission: permissions
  };
  const menu = {
    data: []
  };
  const menuData = await menuRepository.findAll(params);
  menu.data = menuData.data.data;
  return dispatch(setMenu(menu));
}

export const setMenu = (menu) => ({
  type: SET_MENU,
  payload: { menu }
})


export default (state = initialState, { type, payload }) => {
  switch (type) {

    case SET_MENU:
      const { menu } = payload;
      return { ...state, ...menu }

    default:
      return state
  }
}
