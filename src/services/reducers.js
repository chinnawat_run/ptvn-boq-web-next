import { combineReducers } from 'redux';
import authBuyer from './authBuyerService'
import sharedAuth from './sharedAuthService'
import menu from './menuService';
import layout from './layoutService';
import buyerConfig from './buyerConfigService';

export default combineReducers({
  authBuyer,
  sharedAuth,
  menu,
  layout,
  buyerConfig
});
