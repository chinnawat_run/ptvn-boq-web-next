import { SET_BUYER_CONFIG } from "./types";
import { buyerConfigRepository } from '../repositories/BuyerConfigRepository';

const initialState = {
  data: []
}

export const loadBuyerConfig = () => async (dispatch, getState) => {
  const { authBuyer: { user: { buyerMpID } } } = getState();
  const buyerConfig = {
    data: []
  };
  const buyerConfigData = await buyerConfigRepository.getBuyerConfig(buyerMpID);
  buyerConfig.data = buyerConfigData.data;
  return dispatch(setBuyerConfig(buyerConfig));
}

export const setBuyerConfig = (buyerConfig) => ({
  type: SET_BUYER_CONFIG,
  payload: { buyerConfig }
})



export default (state = initialState, { type, payload }) => {
  switch (type) {

    case SET_BUYER_CONFIG:
      const { buyerConfig } = payload;
      return { ...state, ...buyerConfig }

    default:
      return state
  }
}
