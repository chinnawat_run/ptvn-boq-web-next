import { SET_APP_TYPE, SET_PERMISSIONS } from './types';

const initialState = {
  appType: '',
  permissions: []
};

export const setAppType = (appType) => ({
  type: SET_APP_TYPE,
  payload: { appType }
});

export const setPermissions = (permissions) => ({
  type: SET_PERMISSIONS,
  payload: { permissions }
})



export default (state = initialState, { type, payload }) => {
  switch (type) {

  case SET_APP_TYPE:
    return { ...state, ...payload }
  
  case SET_PERMISSIONS:
    return { ...state, ...payload }

  default:
    return state
  }
};
