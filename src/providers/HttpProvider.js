import axios from 'axios';

export const settingAxios = (cookies) => {
  axios.defaults.withCredentials = true;
  if (cookies) {
    axios.defaults.headers.cookie = cookies;
  }
}