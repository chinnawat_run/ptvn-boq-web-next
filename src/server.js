const express = require('express');
const next = require('next');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const EPLandingHelper = require('./api/middlewares/EPLandingHelper');
const { AppType } = require('./commons/constants');
const api = require('./api');

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production';

const nextapp = next({ dev });
const handle = nextapp.getRequestHandler();

nextapp.prepare().then(() => {
  const app = express();

  // app.use(morgan(':id :method :url :status :response-time ms - :res[content-length]', { stream: logger.stream }));
  // app.use(cors());
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(cookieParser());

  api.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, token');
  
    if (true) {
      // Allow to use withCredentials when client requested from different origin
      res.header('Access-Control-Allow-Credentials', true);
      // Must set allow origin to specific domain when requested with withCredentials
      res.header('Access-Control-Allow-Origin', req.headers.origin);
    }
  
    if (req.method === 'OPTIONS') {
      res.sendStatus(200);
    } else {
      next();
    }
  });

  app.use('/api', api);

  app.post('/ManagePriceBook', EPLandingHelper.handleLanding, (req, res) => {
    return nextapp.render(req, res, '/ManagePriceBook');
  });
  app.get('/ManageSupplierGroup', EPLandingHelper.handleLanding, (req, res) => {
    return nextapp.render(req, res, '/ManageSupplierGroup');
  });
  app.get('/ManagePriceModelGroup', EPLandingHelper.handleLanding, (req, res) => {
    return nextapp.render(req, res, '/ManagePriceModelGroup');
  });

  app.all('*', (req, res) => {
    return handle(req, res);
  });

  app.listen(port, err => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});