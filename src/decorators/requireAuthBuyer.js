import nookies from 'nookies';
import { setBuyerUser, setToken } from '../services/authBuyerService';
import { authRepository } from '../repositories/AuthBuyerRepository';

/**
 * load token and check permissions
 * @param {object} ctx
 * @param {object} store
 * @param {string} appType
 * @param {string} token
 * @param {string} permission
 */
export default async (ctx, store, appType, token, permission) => {
  const { data: { data: authData, error } } = await authRepository.loadTokenByBody(token, appType);
  let permissions = [];
  if (error) {
    return false;
  }
  if (authData) {
    permissions = authData.privileges.map(privilege => privilege.privilegeCode);
    if (!permissions.includes(permission)) {
      return false;
    }
    store.dispatch(setBuyerUser(authData));
    store.dispatch(setToken(token))

    nookies.set(ctx, 'buyer_lang', authData.language ? authData.language : 'en', { maxAge: (365 * 24 * 60 * 60) })
  }

  return true;
}
