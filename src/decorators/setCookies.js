import nookies from 'nookies';

export const setAppTypeAndBuyerAuth = (ctx, data, appType) => {
  const token = data.token;
  nookies.set(ctx, 'app_type', appType, { maxAge: 86400 * 1000, httpOnly: true, secure: false });
  if (token) {
    nookies.set(ctx, 'buyer_auth', token, { maxAge: 86400 * 1000, httpOnly: true, secure: false });
  }
}
