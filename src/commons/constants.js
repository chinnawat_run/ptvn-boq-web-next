exports.AppType = Object.freeze({
  BUYER: 'PriceBookBuyer',
  SUPPLIER: 'PriceBookSupplier'
});

exports.AccepFileType = Object.freeze({
  SHEET_TYPE: 'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
});

exports.AlertHeader = Object.freeze({
  ACTION_TAKEN: 'Action Taken:',
  INTERFACE_SAP: 'SAP Module:',
  GR_ALERT_ZERO: 'priceBookCommon:alert'
});

exports.AlertContent = Object.freeze({
  ACTION_TAKEN: 'This invoice has been modified or actioned by another approver.',
  GR_ALERT_ZERO: 'priceBookCommon:allItemAreZero'
});

exports.LeaveApprovalModal = Object.freeze({
  HEADER_TEXT: 'priceBookCommon:leaveWithoutApprove',
  BODY_TEXT: 'priceBookCommon:leaveWithoutApproveBody',
  CONFIRM_TEXT: 'priceBookCommon:leaveWithoutApproveConfirm',
  CANCEL_TEXT: 'priceBookCommon:leaveWithoutApproveCancel'
});

exports.LeaveWithoutCompleting = Object.freeze({
  HEADER_TEXT: 'priceBookCommon:leaveWithoutCompleting',
  BODY_TEXT: 'priceBookCommon:leaveWithoutCompletingBody',
  CONFIRM_TEXT: 'priceBookCommon:leaveWithoutCompletingConfirm',
  CANCEL_TEXT: 'priceBookCommon:leaveWithoutCompletingCancel',
  STAY_TEXT: 'priceBookCommon:stayOnthispage',
  LEAVE_TEXT: 'priceBookCommon:leave'
});

exports.InvoiceStatus = Object.freeze({
  AWAITING_APPROVAL: 1,
  PENDING: 2,
  COMPLETED: 3,
  REJECTED: 4,
  EXPIRED: 5,
  DOCUMENT_RETURN: 6
});

exports.ApprovalType = Object.freeze({
  VERIFY_DOCUMENT: 1,
  ACCOUNT_PAYABLE: 2,
  SPENDING: 3
});

exports.InvoiceType = Object.freeze({
  INVOICE_TYPE_PO_GR: 1,
  INVOICE_TYPE_NONPO_GR: 2,
  INVOICE_TYPE_PO_GR_OFFLINE: 3,
  INVOICE_TYPE_RETENTION: 4
});

exports.BUCKET_NAME_PRICEBOOK = 'boq.pricebook';
