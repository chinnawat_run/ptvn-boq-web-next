/**
 * Place common files likes the vendor library here.
 */

import './assets/sass/frontend_style.scss';

require('../../static/css/common.css');
require('../../static/css/rc-pagination.css');
require('../../static/css/rc-upload.css');